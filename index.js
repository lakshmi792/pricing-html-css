const button = document.getElementById("selector-bar");
button.addEventListener("click", myFunction);

function myFunction() {
    let monthPayment = document.getElementsByTagName("h3");
    let annualPayment = document.getElementsByTagName("h4");
    Array(monthPayment.length).fill(0).map((ele, index) => {
        if (monthPayment[index].style.display === "none") {
            monthPayment[index].style.display = "block";
            annualPayment[index].style.display = "none";
            button.style.justifyContent = "flex-end"
        } else {
            monthPayment[index].style.display = "none";
            annualPayment[index].style.display = "block";
            button.style.justifyContent = "flex-start"
        }

    })
 }
